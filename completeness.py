"""
This module defines the completeness class, which is used for
computing the completeness as a function of star cluster
luminosity.
"""

import numpy as np
from copy import deepcopy
import os.path as osp
from astropy.io import ascii as asc

# Hardcoded bins of apparent magnitude
magbinedge = np.linspace(20, 26, 13)
magbinctr = 0.5*(magbinedge[1:] + magbinedge[:-1])
nbin = 12
dmag = magbinedge[1]-magbinedge[0]

# Completeness class
class completeness(object):
    """
    A completeness object stores information about measured
    completeness in various bands, and uses it to return completeness
    as a function of magnitude.
    """
    
    def __init__(self, basename, filters, dmod, prefix=None, 
                 viscat=False):
        """
        Store the filters, and read completeness data from tables on
        disk. If viscat is True, completeness is computed for the
        visually-confirmed catlog; otherwise it is for the full
        catalog.
        """
        self.filters = deepcopy(filters)
        self.dmod = dmod
        self.viscat = viscat
        self.magedge = magbinedge - dmod
        self.magctr = magbinctr - dmod
        for i, f in enumerate(self.filters):
            if 'F555W' in f:
                self.v_idx = i
            if 'F814W' in f:
                self.i_idx = i
            if 'F435W' in f or 'F438W' in f:
                self.b_idx = i
        self.comp = np.zeros((len(self.filters), nbin))
        for i, f in enumerate(self.filters):
            wl = f.split('_')[-1][1:-1]
            fname = basename+'_f'+wl+'_cmp.txt'
            if prefix is not None:
                fname = osp.join(prefix, fname)
            data = asc.read(fname)
            self.comp[i,:] = data['col2']/100.

    def cmp(self, mag, filt):
        """
        Return the completeness in a particular filter. mag is
        absolute magnitude in mag, and can be a float or an array;
        filt is the filter to use, specified either by giving a name
        or an integer index.
        """
        amag = np.array(mag)
        if type(filt) is str:
            i = self.filters.index(filt)
        else:
            i = filt
        idx = np.clip(np.floor((amag - self.magctr[0]) / dmag).
                      astype('int'), 0, nbin-2)
        wgt = np.clip((amag - self.magctr[idx]) / dmag, -0.5, 1.5)
        val = np.clip(
            (1.0-wgt)*self.comp[i,idx] + wgt*self.comp[i,idx+1],
            0.0, 1.0)
        return val

    def pobs(self, mag, detect=None, filters=None):
        """
        Return the probability that a cluster or a set of clusters
        will be "observed" (i.e., included in the catalog) given a set
        of absolute magnitudes.
        """

        # Make magnitude an array of known shape
        amag = np.array(mag)
        if amag.ndim == 1:
            amag = amag.reshape((1,amag.size))
        ncl = amag.shape[0]

        # If we have no filters or only one set of filters, just pass
        # through to the single-filter routine
        if filters is None:
            if detect is None:
                return self.pobs_filter(amag)
            else:
                return self.pobs_filter(amag[:,detect])
        elif type(filters[0]) is str:
            if detect is None:
                detect = np.zeros(len(self.filters), dtype=bool)
                for i, f in enumerate(self.filters):
                    detect[i] = f in filters
            return self.pobs_filter(amag[:,detect], filters=filters)

        # If we're here, we have multiple sets of filters. Break the
        # data up into blocks that have the same filters, and pass
        # them through to the single-filter routine.
        ncl = len(mag)
        if len(filters) != ncl:
            raise ValueError, "number of clusters must = " + \
                "number of filter sets"
        pobs = np.zeros(ncl)
        pobs[:] = -1.0
        while np.any(pobs < 0):
            # Find first cluster not yet processed, and get the
            # filters for it
            idx = np.argmax(pobs < 0)
            fset = filters[idx]
            # Find all filters that match this set
            idx1 = [idx]
            for i, f in enumerate(filters[idx+1:]):
                if f == fset:
                    idx1.append(i+idx+1)
            # Figure out which parts of the magnitude array to use for
            # this combination of filters
            if detect is not None:
                dt = detect[idx1][0]
            else:
                dt = np.zeros(len(self.filters), dtype=bool)
                for i, f in enumerate(self.filters):
                    dt[i] = f in fset
            # Evaluate probabilities for this filter set
            pobs[idx1] = self.pobs_filter(
                amag[idx1][:,dt], filters=fset)
        # Return
        return pobs


    def pobs_filter(self, mag, filters=None):
        """
        Return the probability that a cluster or a set of clusters
        will be "observed" (i.e., included in the catalog) given a set
        of absolute magnitudes. This differs from pobs in that filter
        must be a single set of filters, not one per cluster.
        """

        # Make magnitude an array of known shape
        amag = np.array(mag)
        if amag.ndim == 1:
            amag = amag.reshape((1,amag.size))
        ncl = amag.shape[0]

        # Figure out which filters we're using; fidx is the mapping
        # between the input filters and the stored filters, while
        # vidx, iidx, and bidx are the indices of the V, I, and B
        # filters, respectively
        if filters is None:
            fidx = np.arange(len(self.filters))
            vidx = self.v_idx
            iidx = self.i_idx
            bidx = self.b_idx
        else:
            if type(filters[0]) is str:
                fidx = np.zeros(len(filters), dtype='int')
                for i, f in enumerate(filters):
                    fidx[i] = self.filters.index(f)
            else:
                fidx = filters
            vidx = np.where(fidx == self.v_idx)
            iidx = np.where(fidx == self.i_idx)
            bidx = np.where(fidx == self.b_idx)
            if len(vidx) == 0 or len(iidx) == 0 or len(bidx) == 0:
                return 0.0
            else:
                vidx = vidx[0][0]
                iidx = iidx[0][0]
                bidx = bidx[0][0]

        # Get detection probability in all bands
        pdetect = np.zeros(amag.shape)
        for i, f in enumerate(fidx):
            pdetect[:,i] = self.cmp(amag[:,i], f)

        # Loop over possible combinations of detections and
        # non-detections in each band. A cluster is included in the
        # full catalog only if it has:
        #
        # (1) A detection in V
        # (2) A detection in either I or B
        # (3) A detection in at least 3 bands total
        #
        # For the visual catalog we require:
        #
        # (1) A detection in V
        # (2) A detection in either I or B
        # (3) A detection in at least 4 bands
        # (4) V magnitude < -6
        #
        # For cases that satisfy these conditions, add up the
        # probabilities
        prob = np.zeros(ncl)
        for i in range(2**fidx.size):
            # i is interpreted bitwise, with a 1 in a given bit
            # corresponding to a detection in that band

            # Do we have a detection in V? If not, move on.
            vdetect = i & (1 << vidx)
            if not vdetect:
                continue

            # Now check for a detection in B or I; if we have neither
            # of those, move on
            bdetect = i & (1 << bidx)
            idetect = i & (1 << iidx)
            if not (bdetect or idetect):
                continue

            # Now get number of bands detected; if it's too small,
            # continue
            ndetect = bin(i).count('1')
            if ndetect < 3:
                continue

            # If we're using the visual catalog, check that we have 4
            # bands
            if self.viscat and ndetect < 4:
                continue

            # This case does get included in the catalog, so compute
            # its probability
            prob_case = np.ones(ncl)
            for j in range(fidx.size):
                if i & (1 << j):
                    prob_case *= pdetect[:,j]
                else:
                    prob_case *= 1.0 - pdetect[:,j]
            prob += prob_case

        # If using the visual catalog, set pdetect = 0 for any cluster
        # with V magnitude > -6
        if self.viscat:
            prob[amag[:,vidx] > -6] = 0.0
        
        # Return
        return prob
