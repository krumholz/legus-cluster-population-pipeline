"""
Function to read photometry files in the standard LEGUS format and
their associated metadata and return their contents in a useful form.
"""

from astropy.io import ascii as asc
import os.path as osp
import numpy as np

def readphot(fname, classcut=[0, 3.5], metaonly=False, verbose=False):

    # Print status
    if verbose:
        print("Reading photometry from {:s}...".format(f))

    # Read the metadata; this lists the distance mod and filters for
    # the file
    fmeta = osp.splitext(fname)[0]+'.dat'
    fp = open(fmeta, 'r')
    dmod = float(fp.readline())
    allfilters = fp.read().split()
    fp.close()

    # If just returning the metadata, do that now
    if metaonly:
        photdat = { 'name' : osp.basename(fname).split('_')[0], 
                    'distmod' : dmod, 'allfilters' : allfilters }
        return photdat

    # Read the photometry
    data = asc.read(fname)
    cid = data['col1']
    nc = len(cid)
    nf = len(allfilters)
    phot = np.zeros((nc, nf))
    photerr = np.zeros((nc, nf))
    detect = np.ones((nc, nf), dtype=bool)
    for i in range(nf):
        phot[:,i] = data['col{:d}'.format(2*i+6)] - dmod
        photerr[:,i] = data['col{:d}'.format(2*i+7)]
        detect[:,i] = data['col{:d}'.format(2*i+6)] < 99.  # Non-detection flag
    ra = np.array(data['col4'])
    dec = np.array(data['col5'])
    mfit = np.array(data['col20'])
    tfit = np.array(data['col17'])
    ebvfit = np.array(data['col23'])

    # Throw out all objects outside the allowed classification range
    classification = data['col34']
    idx = np.logical_and(classification > classcut[0], 
                         classification < classcut[1])
    cid = cid[idx]
    phot = phot[idx]
    photerr = photerr[idx]
    detect = detect[idx]
    ra = ra[idx]
    dec = dec[idx]
    mfit = mfit[idx]
    tfit = tfit[idx]
    ebvfit = ebvfit[idx]

    # Make a list of the filters to be used for each cluster
    filters = []
    for dt in detect:
        filtertmp = []
        for d, f1 in zip(dt, allfilters):
            if d:
                filtertmp.append(f1)
        filters.append(filtertmp)

    # Make a list of all the unique filter sets present in the data,
    # and record which one of them is used by each cluster
    filtersets = []
    for f in filters:
        if not f in filtersets:
            filtersets.append(f)
    cfilterset = np.array([filtersets.index(f) for f in filters])

    # Make a dict of the results and return
    photdat = { 'name' : osp.basename(fname).split('_')[0], 
                'distmod' : dmod, 'filters' : filters, 'cid' : cid, 
                'phot' : phot, 'photerr' : photerr, 
                'detect' : detect, 'ra' : ra,
                'dec' : dec, 'allfilters' : allfilters,
                'filtersets' : filtersets, 'cfilterset' : cfilterset,
                'mfit' : mfit, 'tfit' : tfit, 'ebvfit' : ebvfit }
    return photdat
