from slugpy.cluster_slug import cluster_slug
import numpy as np
import argparse
import os.path as osp
from ctypes import c_double
from multiprocessing import Pool, Value, cpu_count, current_process
from readphot import readphot

# Parse the inputs
parser = argparse.ArgumentParser(
    description="Script to do first step in deriving posterior PDFs on galaxy star cluster properties")
parser.add_argument('photfiles', nargs='+', default=None,
                    help='list of photometry files to use')
parser.add_argument('-l', '--libname', default='modp020_kroupalim_MW_phi0.50',
                    help='name of cluster_slug library to use')
parser.add_argument('-o', '--outdir', default=None,
                    help='name of output directory; default is current dir')
parser.add_argument('-p', '--photsystem', default='Vega',
                    help='photometric system used for data')
parser.add_argument('-msl', '--massslopelim', nargs=2,
                    default=[-3.0, -1.0], type=float,
                    help='lower and upper limits on the possible slope in mass')
parser.add_argument('-asl', '--ageslopelim', nargs=2,
                    default=[-2.0, 0.0], type=float,
                    help='lower and upper limits on the possible slope in age')
parser.add_argument('--nproc', default=-1, type=int,
                    help='number of processors to use (default: all)')
parser.add_argument('-v', '--verbose', default=False,
                    action='store_true',
                    help='produce verbose output')
args = parser.parse_args()
#args = parser.parse_args('phot/ngc628c_cluster_legus_avgapcor_PadAGB_MWext_16July15.tab --outdir /Users/krumholz/Data/legus/clusterpop --libname /Users/krumholz/Data/legus/lib/modp020_kroupalim_MW_phi0.50 -v'.split())

# Read the input photometry files
photdat = []
for f in args.photfiles:
    photdat.append(readphot(f))

# Define the sample density for the cluster_slug library
if osp.basename(args.libname) == 'modp020_kroupalim_MW_phi0.50' or \
   osp.basename(args.libname) == 'modp020_kroupalim_SB_phi0.50' or \
   osp.basename(args.libname) == 'modp004_kroupalim_MW_phi0.50' or \
   osp.basename(args.libname) == 'modp008_kroupalim_MW_phi0.50' or \
   osp.basename(args.libname) == 'modp020_chabrier_MW_phi0.50' or \
   osp.basename(args.libname) == 'Z0140v00_kroupalim_MW_phi0.50':
    def sample_density(physprop):
        logm = physprop[:,0]
        logt = physprop[:,1]
        sden = np.ones(len(logm))
        sden[logm > 4] = sden[logm > 4] * 1.0/10.**(logm[logm > 4]-4)
        sden[logt > 8] = sden[logt > 8] * 1.0/10.**(logt[logt > 8]-8)
        return sden
else:
    print("Unrecognized file name, using uniform sample density")
    sample_density = None

# Define a function for priors
class priorfunc(object):
    def __init__(self, massslope, ageslope, logageflat=6.5):
        self.massslope = massslope
        self.ageslope = ageslope
        self.logtflat = logageflat
    def prior(self, physprop):
        logm = physprop[:,0]
        logt = physprop[:,1]
        massprior = (10.**logm)**(self.massslope+1)
        timeprior = (10.**(logt-self.logtflat))**(self.ageslope+1)
        timeprior[logt < self.logtflat] \
            = 10.**(logt[logt < self.logtflat]-self.logtflat)
        return massprior*timeprior

# Create the cluster_slug object
if args.verbose:
    print("Initializing cluster_slug object...")
cs = cluster_slug(photsystem=args.photsystem, libname=args.libname,
                  sample_density = sample_density,
                  abstol = 1.0e-10, bw_phot = 0.1)

# Load all the filter sets we'll need
for ph in photdat:
    for f in ph['filters']:
        if f not in cs.filtersets():
            if args.verbose:
                outstr = "Constructing kernel density estimation for filters"
                for f1 in f:
                    outstr = outstr + " " + f1
                print(outstr)
            cs.add_filters(f)

# Define a function that returns the kernel density representation of
# the data for a specified set of photometric values and errors; also
# define some global counters to go with it
ndone = Value('i', 0)
ncl = Value('i', 0)
galidx = Value('i', 0)
def make_approx_phys(clidx):
    if args.verbose:
        with ndone.get_lock():
            print(("{:s}: making kernel density representation for" +
                   " galaxy {:d}/{:d}, cluster {:d}/{:d}").
                  format(current_process().name, galidx.value+1,
                         len(photdat), ndone.value+1, ncl.value))
            ndone.value = ndone.value+1
            ndone_save = ndone.value
    detect = photdat[galidx.value]['detect'][clidx]
    ret = cs.make_approx_phys(
        photdat[galidx.value]['phot'][clidx][detect],
        photerr=photdat[galidx.value]['photerr'][clidx][detect],
        filters=photdat[galidx.value]['filters'][clidx], 
        squeeze=False)
    return ret

# Define a function for taking the 4 limiting cases and extracting the
# points the occur in any of them
def extract_rep(kdrep):

    # Handle the cases where we hit a numerical error by returning two
    # length-zero arrays
    if None in kdrep:
        return np.zeros(0), np.zeros(0)

    # Make arrays that combine all the points returned for each set of
    # priors
    xall = np.concatenate([kd[0] for kd in kdrep], axis=0)
    wgtall = np.concatenate([kd[1] for kd in kdrep])

    # Sort the points by mass, age, and A_V
    idxsort = np.lexsort((xall[:,2], xall[:,1], xall[:,0]))
    xall = xall[idxsort,:]
    wgtall = wgtall[idxsort]

    # Now eliminate duplicates by finding points that are
    # identical to the ones before them
    while True:
        unq = np.logical_not(np.all(
            np.equal(xall[:-1,:], xall[1:,:]), axis=1))
        unq = np.append(unq, True)
        if np.sum(unq) < len(xall):
            xall = xall[unq,:]
            wgtall = wgtall[unq]
        else:
            break

    # Return the extracted point
    return xall, wgtall

# Start master thread
#if 1:
if __name__ == '__main__':

    # Create the parallel process pool
    if args.nproc <= 0:
        if args.verbose:
            print "Starting {:d} processes".format(cpu_count())
        pool = Pool()
    else:
        if args.verbose:
            print "Starting {:d} processes".format(args.nproc)
        pool = Pool(processes=args.nproc)

    # Loop over galaxies
    for gidx, ph in enumerate(photdat):

        # Set the galaxy index for all workers
        galidx.value = gidx

        # Prepare the output holder; initially this is 4 lists of
        # nphot elements that are all None
        kdrep = [4*[None] for n in range(len(ph['phot']))]

        # Get number of clusters
        ncl.value = len(ph['phot'])

        # Loop over the limits on the search range
        ctr = 0
        for ms in args.massslopelim:
            for ts in args.ageslopelim:

                # Print status message
                if args.verbose:
                    print(("Making representations for {:s}, priors "+
                           "beta = {:f}, gamma = {:f}").
                          format(ph['name'], ms, ts))
                    ndone.value = 0

                # Set the prior
                prior = priorfunc(ms, ts).prior
                cs.priors = prior

                # Compute result in parallel
                kdrep_tmp = pool.map(make_approx_phys, range(ncl.value))
                #kdrep_tmp = map(make_approx_phys, range(ncl.value))

                # Adjust the weights by dividing out the priors, and
                # store the final result
                for i, kdrp in enumerate(kdrep_tmp):
                    if kdrp is not None:
                        kdrp[1][:] = kdrp[1]/prior(kdrp[0])
                        kdrep[i][ctr] = kdrp

                # Update counter
                ctr = ctr+1

        # We now have the kernel representation for every cluster and
        # at the extreme limits of the priors we're considering. Now
        # find the unique points for all four sets of priors.
        if args.verbose:
            print("Merging representations for {:s}".
                  format(ph['name']))
        kdrep_unq = pool.map(extract_rep, kdrep)
        #kdrep_unq = map(extract_rep, kdrep)

        # Make this into a dict we can use to write; we could do this
        # more elegantly with a dict comprehension, but for some older
        # versions of python this creates problems, so we do it with
        # list comprehensions and a zip instead
        xkeys = [ 'x'+str(ph['cid'][i]) for i in range(ncl.value) ]
        wkeys = [ 'w'+str(ph['cid'][i]) for i in range(ncl.value) ]
        xvals = [ kdrep_unq[i][0] for i in range(ncl.value) ]
        wvals = [ kdrep_unq[i][1] for i in range(ncl.value) ]
        keys = xkeys + wkeys
        vals = xvals + wvals
        kdrep_out = dict(zip(keys, vals))

        # Write output to file
        outfile = ph['name']+'_kdrep.npz'
        if args.outdir is not None:
            outfile = osp.join(args.outdir, outfile)
        if args.verbose:
            print("Saving representations to {:s}".format(outfile))
        np.savez(outfile, **kdrep_out)

