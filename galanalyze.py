import numpy as np
from numpy.random import rand
import argparse
import os.path as osp
from subprocess import call
from ctypes import c_double, c_long
from multiprocessing import cpu_count, Value, RawArray, Pool
from readphot import readphot
import emcee
from slugpy import read_cluster
from slugpy.cluster_slug import cluster_slug
from completeness import completeness
import time
import sys

# Parse the inputs
parser = argparse.ArgumentParser(
    description="Script to do derive posterior PDFs on " +
    "galaxy star cluster properties")
parser.add_argument('photfiles', nargs='+', default=None,
                    help='list of photometry files to use')
parser.add_argument('-f', '--fullcatalog', default=False,
                    action='store_true',
                    help='use full catalog (default is visually ' +
                    'confirmed clusters only)')
parser.add_argument('-ea', '--excludeassoc', default=False,
                    action='store_true',
                    help='exclude "associations" (class 3 objects)'
                    ' from the analysis')
parser.add_argument('-l', '--libname', default='modp020_kroupalim_MW_phi0.50',
                    help='name of cluster_slug library to use')
parser.add_argument('-kd', '--kddir', default=None,
                    help='name of the directory where kernel density '
                    'representation files are located')
parser.add_argument('-o', '--outname', default="chain.dat",
                    help='name of output file; default is chain.dat')
parser.add_argument('-p', '--photsystem', default='Vega',
                    help='photometric system used for data')
parser.add_argument('-msl', '--massslopelim', nargs=2,
                    default=[-3.0, -1.0], type=float,
                    help='lower and upper limits on the possible slope'
                    ' in mass')
parser.add_argument('-asl', '--ageslopelim', nargs=2,
                    default=[-3.0, 0.0], type=float,
                    help='lower and upper limits on the possible slope'
                    ' in age')
parser.add_argument('--nproc', default=-1, type=int,
                    help='number of processors to use (default: all)')
parser.add_argument('--nav', default=7, type=int,
                    help='number of points to parameterise the A_V '
                    'distribution')
parser.add_argument('--ntbreak', default=2, type=int,
                    help='number of breakpoints in the powerlaw age '
                    'distribution')
parser.add_argument('-mc', '--mincomp', default=0.2, type=float,
                    help='minimum completeness to include')
parser.add_argument('-r', '--restart', default=False, action='store_true',
                    help='restart MCMC from the output file')
parser.add_argument('-v', '--verbose', default=False,
                    action='store_true',
                    help='produce verbose output')
#args = parser.parse_args()
args = parser.parse_args('phot/ngc628c_cluster_legus_avgapcor_PadAGB_MWext_16July15.tab phot/ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab -kd /Users/krumholz/Data/legus/clusterpop --libname /Users/krumholz/Data/legus/lib/modp020_kroupalim_MW_phi0.50 -v --ageslopelim -3 0 --restart --outname ngc628_chain_incassoc_nt2_nav7_mc0.2.dat'.split())

# Step 1: make KD rep files for all clusters if we don't have them
# already; this is handled by the galprep.py script; be sure to call
# the same version of python we were called with, to avoid breaking
# things when the default python path points to an obsolete install
for f in args.photfiles:
    kdfile = osp.basename(f).split('_')[0]+'_kdrep.npz'
    if args.kddir is not None:
        kdfile = osp.join(args.kddir, kdfile)
    if not osp.isfile(kdfile):
        if args.verbose:
            print "Generating kdrep file "+kdfile+" for "+f
        cmd = (sys.executable + ' galprep.py ' + f + 
               ' --libname ' + args.libname +
               ' --photsystem ' + args.photsystem +
               ' --massslopelim {:f} {:f}'.format(
                   float(args.massslopelim[0]),
                   float(args.massslopelim[1])) +
               ' --ageslopelim {:f} {:f}'.format(
                   float(args.ageslopelim[0]),
                   float(args.ageslopelim[1])) +
               ' --nproc {:d}'.format(args.nproc))
        if args.kddir is not None:
            cmd = cmd + ' --outdir '+args.kddir
        if args.verbose:
            cmd = cmd + ' --verbose'
        call(cmd, shell=True)   


# Step 2: read the cluster_slug library we'll be using, and store the
# data in RawArray object to allow parallel read-only access; also
# store some limits on the data, which will be used to fence the MCMC
if args.verbose:
    print "Reading slug simulation library {:s}".format(args.libname)
libdat = read_cluster(args.libname)
m_lib_base = RawArray(c_double, len(libdat.actual_mass))
m_lib = np.frombuffer(m_lib_base, dtype=c_double)
m_lib[:] = libdat.actual_mass
t_lib_base = RawArray(c_double, len(libdat.time))
t_lib = np.frombuffer(t_lib_base, dtype=c_double)
t_lib[:] = libdat.time
av_lib_base = RawArray(c_double, len(libdat.A_V))
av_lib = np.frombuffer(av_lib_base, dtype=c_double)
av_lib[:] = libdat.A_V
logmlim = np.log10(np.array([np.amin(m_lib), np.amax(m_lib)]))
logtlim = np.log10(np.array([np.amin(t_lib), np.amax(t_lib)]))
avlim = [np.amin(av_lib), np.amax(av_lib)]
delta_av = (avlim[1] - avlim[0]) / (args.nav - 1)


# Step 3: compute sample density for cluster_slug library, and store
# that in a RawArray too
sden = np.ones(len(m_lib))
if osp.basename(args.libname) == 'modp020_kroupalim_MW_phi0.50' or \
   osp.basename(args.libname) == 'modp020_kroupalim_SB_phi0.50' or \
   osp.basename(args.libname) == 'modp004_kroupalim_MW_phi0.50' or \
   osp.basename(args.libname) == 'modp008_kroupalim_MW_phi0.50' or \
   osp.basename(args.libname) == 'modp020_chabrier_MW_phi0.50' or \
   osp.basename(args.libname) == 'Z0140v00_kroupalim_MW_phi0.50':
    sden[m_lib > 1e4] = sden[m_lib > 1e4] / (m_lib[m_lib > 1e4]/1e4)
    sden[t_lib > 1e8] = sden[t_lib > 1e8] / (t_lib[t_lib > 1e8]/1e8)
else:
    print("Unrecognized file name, using uniform sample density")
wgt_lib_base = RawArray(c_double, len(sden))
wgt_lib = np.frombuffer(wgt_lib_base, dtype=c_double)
wgt_lib[:] = 1/sden


# Step 4: read metadata for input photometry files, just so we know
# what filters we're dealing with
allfilters = []
for f in args.photfiles:
    data = readphot(f, metaonly=True)
    for flt in data['allfilters']:
        if flt not in allfilters:
            allfilters.append(flt)
nf = len(allfilters)


# Step 5: read the input photometry files; apply cuts to the
# classification and remove duplicates to produce the final observed
# photometry list we'll be using; in the process, compute the
# observational completeness for each cluster, and for the library at
# each possible combination of field and filter set, and read the
# kernel density representations
radec = []
phot = np.zeros((0,nf))
photerr = np.zeros((0,nf))
detect = np.zeros((0,nf), dtype=bool)
mfit = np.zeros(0)
ffidx_tmp = np.zeros(0, dtype=int)
ffinfo = []
p_obs_dat_tmp = np.zeros(0)
p_obs_lib_tmp = np.zeros((0, libdat.phot_neb_ex.shape[0]))
nread = 0
n_kd = 0
cid_to_use = []
for f in args.photfiles:

    # Read photometry of the clusters we want
    if args.verbose:
        print "Reading photometry file {:s}".format(f)
    if args.fullcatalog:
        data = readphot(f, classcut=[-1,3.5])
    elif not args.excludeassoc:
        data = readphot(f, classcut=[0,3.5])
    else:
        data = readphot(f, classcut=[0,2.5])
    nread += len(data['phot'])

    # Build the completeness object for this field and get
    # completeness estimate for all clusters
    if args.verbose:
        print "Computing completeness"
    cmpl = completeness(data['name'].split('_')[0], 
                        data['allfilters'],
                        data['distmod'], prefix='cmpdat', 
                        viscat=not args.fullcatalog)
    p_obs_dat_file = cmpl.pobs(data['phot'], detect=data['detect'],
                               filters=data['filters'])

    # Read the KD representation for this field
    kdfile = data['name']+'_kdrep.npz'
    if args.kddir is not None:
        kdfile = osp.join(args.kddir, kdfile)
    kd = np.load(kdfile, mmap_mode='r')

    # Now we do quality control on the data; we want to throw out:
    # (1) clusters that are duplicates of ones stored in other files;
    # (2) clusters so far from the library that we couldn't build
    #     meaningful KD representations for them
    # (3) clusters that are so far down the completeness that we don't
    #     believe we can make a reasonable completeness correction for
    #     them

    # Check RA and DEC against already-ingested clusters
    unqflag = np.zeros(len(data['phot']), dtype='bool')
    rad = zip(data['ra'], data['dec'])
    for i, rd in enumerate(rad):
        if rd not in radec:
            unqflag[i] = True
            radec.append(rd)

    # Check that clusters have KD representations that are not None
    kdflag = np.zeros(len(data['phot']), dtype='bool')
    for i, cid in enumerate(data['cid']):
        kdflag[i] = kd['w'+str(cid)].size > 0

    # Check that completeness is acceptable
    cmpflag = p_obs_dat_file > args.mincomp
    
    # Construct the set of clusters that pass all quality criteria;
    # count how many of them there are, and the number of points in
    # their KD representations
    goodidx = np.logical_and.reduce((unqflag, kdflag, cmpflag))
    ncl = np.sum(goodidx)
    for i, cid in enumerate(data['cid']):
        if goodidx[i]:
            n_kd += kd['w'+str(cid)].size    

    # Print status
    if args.verbose:
        print "Read {:d} clusters:".format(len(data['phot']))
        print "   {:d} duplicates".\
            format(np.sum(np.logical_not(unqflag)))
        print "   {:d} bad KD representations".\
            format(np.sum(np.logical_not(kdflag)))
        print "   {:d} below completeness cut".\
            format(np.sum(np.logical_not(cmpflag)))
        print "Retaining {:d}".format(ncl)
        
    # Figure out the mapping between the numerical indices of the
    # filters used for this photometry file and the global indexing
    # we're using for the merged photometry over all files
    fidx = np.zeros(len(data['allfilters']), dtype=int)
    for i in range(fidx.size):
        fidx[i] = allfilters.index(data['allfilters'][i])

    # Arrange the photometric information for this field and add it to
    # the global photometry list
    phottmp = np.zeros((ncl, nf))
    photerrtmp = np.zeros((ncl, nf))
    detecttmp = np.zeros((ncl, nf), dtype=bool)
    mfittmp = data['mfit'][goodidx]
    for i, j in enumerate(fidx):
        phottmp[:,j] = data['phot'][goodidx,i]
        photerrtmp[:,j] = data['photerr'][goodidx,i]
        detecttmp[:,j] = data['detect'][goodidx,i]
    phot = np.concatenate((phot, phottmp))
    photerr = np.concatenate((photerr, photerrtmp))
    detect = np.concatenate((detect, detecttmp))
    mfit = np.concatenate((mfit, mfittmp))

    # Record the IDs of the clusters we're using
    cid_to_use.append(data['cid'][goodidx])

    # Store the observational completeness estimate for the data
    p_obs_dat_tmp = np.concatenate((p_obs_dat_tmp,
                                    p_obs_dat_file[goodidx]))

    # Now we need to get the observational completeness for the
    # library. We have to evaluate this for every combination of field
    # and filter set, so, as a first step, figure out how many
    # distinct filter sets are present in this field, and record
    # information describing them
    for fs in data['filtersets']:
        ffinfo.append((data['name'], fs))

    # For each cluster, record which combination of field and data set
    # it is using
    ffidxtmp = []
    for g in np.where(goodidx)[0]:
        ffidxtmp.append(
            ffinfo.index((data['name'], data['filters'][g])))
    ffidx_tmp = np.concatenate((ffidx_tmp, ffidxtmp))

    # Now get the observational completeness of the library clusters
    # for all filter sets in this field; note that we need to extract
    # from the library just those filters that match the ones used for
    # this field
    if args.verbose:
        print "Getting observational completeness for slug library"
    libphot = np.zeros((libdat.phot_neb_ex.shape[0],
                        len(data['allfilters'])))
    for i, f in enumerate(data['allfilters']):
        libphot[:,i] \
            = libdat.phot_neb_ex[:,libdat.filter_names.index(f)]
    for fs in data['filtersets']:
        p_obs_lib_file = cmpl.pobs(libphot, filters=fs).\
                         reshape(1, len(libphot))
        p_obs_lib_file[p_obs_lib_file < args.mincomp] = 0.0
        p_obs_lib_tmp = np.concatenate(
            (p_obs_lib_tmp, p_obs_lib_file))
        

# Step 6: read in the kernel density representation data. We do this
# in two passes, one to count how much storage we'll need, and a
# second to actually store the data

# Prepare memory
n_cl = len(phot)        # Number of clusters in the final data set
kdw_base = RawArray(c_double, n_kd)
kdx_base = RawArray(c_double, n_kd*5)
kdbrks_base = RawArray(c_long, n_cl+1)
kdx = np.frombuffer(kdx_base, dtype=c_double).reshape((n_kd,5))
kdw = np.frombuffer(kdw_base, dtype=c_double)
kdbrks = np.frombuffer(kdbrks_base, dtype=c_long)
ptr1 = 0
ptr2 = 1
kdbrks[0] = 0

# Print stats
print "Read {:d} clusters, using {:d} clusters".format(nread, n_cl)

# Loop over fields
for f, cid_field in zip(args.photfiles, cid_to_use):

    # Read the KD representation for this field
    kdfile = osp.basename(f).split('_')[0]+'_kdrep.npz'
    if args.kddir is not None:
        kdfile = osp.join(args.kddir, kdfile)
    if args.verbose:
        print "Loading KD representation file {:s}".format(kdfile)
    kd = np.load(kdfile)

    # Import data
    for cid in cid_field:
        scid = str(cid)
        kdw[ptr1:ptr1+len(kd['w'+scid])] = kd['w'+scid]
        kdx[ptr1:ptr1+len(kd['x'+scid]),:3] = kd['x'+scid]
        kdx[ptr1:ptr1+len(kd['x'+scid]),3] \
            = 10.**kdx[ptr1:ptr1+len(kd['x'+scid]),0]
        kdx[ptr1:ptr1+len(kd['x'+scid]),4] \
            = 10.**kdx[ptr1:ptr1+len(kd['x'+scid]),1]
        kdbrks[ptr2] = ptr1 + len(kd['w'+scid])
        ptr1 += len(kd['w'+scid])
        ptr2 += 1

        
# Step 7: pack all the data we'll need later into shared arrays, and
# free redundant memory

# Count number of objects
if args.verbose:
    print "Doing shared memory packing"
n_lib = libdat.phot_neb_ex.shape[0]   # Number of library clusters
n_ff = len(ffinfo)      # Number of distinct field / filter set combos

# Allocate from shared memory
p_obs_dat_base = RawArray(c_double, n_cl)
p_obs_lib_base = RawArray(c_double, n_lib*n_ff)
ffidx_base = RawArray(c_long, n_cl)
p_obs_dat = np.frombuffer(p_obs_dat_base, dtype=c_double)
p_obs_lib = np.frombuffer(p_obs_lib_base, dtype=c_double). \
            reshape(n_ff, n_lib)
ffidx = np.frombuffer(ffidx_base, dtype=c_long)

# Copy to shared memory
p_obs_dat[:] = p_obs_dat_tmp
p_obs_lib[:,:] = p_obs_lib_tmp
ffidx[:] = ffidx_tmp

# Free memory
p_obs_dat_tmp = None
p_obs_lib_tmp = None
ffidx_tmp = None
kd = None
libdat = None

# Define a function that returns the log likelihood, computed from the
# shared arrays
def lnprob(params):

    # Parameters are, in order:
    # logmstar
    # mslope
    # logtbreak[ntbreak]
    # tslope[ntbreak]
    # p_av[nav-1]
    logmstar = params[0]
    mslope = params[1]
    logtbreak = params[2:2+args.ntbreak]
    tslope = params[2+args.ntbreak:2+2*args.ntbreak]
    p_av = params[2+2*args.ntbreak:2+2*args.ntbreak+args.nav-1]

    # Fill in last element of p_av by conservation of probability
    p_av = np.append(p_av,
                     [2.0/delta_av - p_av[0] -
                      2.0*np.sum(p_av[1:])])

    # Return -inf if we are outside the allowed bounds
    if (logmstar < logmlim[0] - 1) or \
       (logmstar > logmlim[1] + 1) or \
       (mslope > args.massslopelim[1]) or \
       (mslope < args.massslopelim[0]) or \
       (np.sum(logtbreak < logtlim[0]-1) > 0) or \
       (np.sum(logtbreak > logtlim[1]+1) > 0) or \
       (np.sum(tslope > args.ageslopelim[1]) > 0) or \
       (np.sum(tslope < args.ageslopelim[0]) > 0) or \
       (np.sum(p_av < 0) > 0):
        return -np.inf

    # Force tbreak to be strictly increasing
    if args.ntbreak > 1:
        if np.sum(logtbreak[1:] <= logtbreak[:-1]) > 0:
            return -np.inf

    # Start clock
    if args.verbose:
        tstart = time.clock()

    # Set up shorthands
    logm = kdx[:,0]
    logt = kdx[:,1]
    av = kdx[:,2]
    m = kdx[:,3]
    t = kdx[:,4]
    ln10 = np.log(10)
    mstar = 10.**logmstar
    tbreak = 10.**logtbreak

    # Compute mass weights for library and observations
    wm = (m/mstar)**(mslope+1)*np.exp(-m/mstar)
    wmlib = (m_lib/mstar)**(mslope+1)*np.exp(-m_lib/mstar)

    # Age weights
    wt = np.zeros(len(t))
    wt[t <= tbreak[0]] = t[t <= tbreak[0]] / tbreak[0]
    wtlib = np.zeros(len(t_lib))
    wtlib[t_lib <= tbreak[0]] = t_lib[t_lib <= tbreak[0]] / tbreak[0]
    tcoef = 1.0
    for i in range(tbreak.size-1):
        idx = np.logical_and(t > tbreak[i], t <= tbreak[i+1])
        wt[idx] = tcoef * (t[idx]/tbreak[i])**(tslope[i]+1)
        idx = np.logical_and(t_lib > tbreak[i], t_lib <= tbreak[i+1])
        wtlib[idx] = tcoef * (t_lib[idx]/tbreak[i])**(tslope[i]+1)
        tcoef *= (tbreak[i+1]/tbreak[i])**(tslope[i]+1)
    wt[t > tbreak[-1]] = tcoef * \
                         (t[t > tbreak[-1]]/tbreak[-1]) \
                         **(tslope[-1]+1)
    wtlib[t_lib > tbreak[-1]] = tcoef * \
                                (t_lib[t_lib > tbreak[-1]]/tbreak[-1]) \
                                **(tslope[-1]+1)

    # AV
    wav = np.zeros(len(av))
    wavlib = np.zeros(len(av_lib))
    for i in range(args.nav-1):
        avlo = avlim[0] + i*delta_av
        avhi = avlim[0] + (i+1)*delta_av
        idx = np.logical_and(av >= avlo, av <= avhi)
        wav[idx] = p_av[i] * (avhi - av[idx]) / delta_av + \
                   p_av[i+1] * (av[idx] - avlo) / delta_av
        idx = np.logical_and(av_lib >= avlo, av_lib <= avhi)
        wavlib[idx] = p_av[i] * (avhi - av_lib[idx]) / delta_av + \
                      p_av[i+1] * (av_lib[idx] - avlo) / delta_av

    # Total weights and normalisation
    wprod = wm*wt*wav*kdw
    norm = 1.0 / np.sum(wmlib*wtlib*wavlib*wgt_lib*p_obs_lib, axis=1)
    
    # Map the normalizations onto the clusters
    cnorm = norm[ffidx]
    
    # Compute the log of the likelihood function
    lnprob = np.atleast_1d(
        np.log(cnorm[0]*p_obs_dat[0]*
               np.sum(wprod[kdbrks[0]:kdbrks[1],...], 
                      axis=0)))
    for i in range(1,kdbrks.size-1):
        lnprob += np.log(p_obs_dat[i]*cnorm[i]*np.sum(
            wprod[kdbrks[i]:kdbrks[i+1],...], axis=0))

    # Safety measure; if we got NaN because we're in some bizarre part
    # of parameter space, change to -inf
    lnprob[np.isnan(lnprob)] = -np.inf

    # Reduce dimensions of lnprob
    lnprob = np.squeeze(lnprob)

    # Increment call counter and print if verbose
    if args.verbose:
        tend = time.clock()
        with callctr.get_lock():
            print("Call {:d}: ".format(callctr.value) +
                  "logmstar = "+str(logmstar) +
                  ", mslope = "+str(mslope) +
                  ", logtbreak = "+str(logtbreak) +
                  ", tslope = "+str(tslope) +
                  ", p_av = "+str(p_av) +
                  "; lnprob = " + str(lnprob) +
                  "  ---  evaluation time = "+str(tend-tstart))
            callctr.value += 1
            
    # Return
    if lnprob.size == 1:
        return np.asscalar(lnprob)
    else:
        return lnprob


# MCMC parameters
nwalkers = 100
ndim = 2 + 2*args.ntbreak + args.nav-1
niter = 500

# Set initial walker positions
p0 = np.zeros((nwalkers, ndim))
if not args.restart:
    nread = 0
    ptr = 0
    p0[:,ptr] = 6 + 2*(rand(nwalkers)-0.5)
    ptr += 1
    p0[:,ptr] = -2 + rand(nwalkers)-0.5
    ptr += 1
    p0[:,ptr] = 6.5 + rand(nwalkers)-0.5
    ptr += 1
    for i in range(args.ntbreak-1):
        p0[:,ptr] = 6.5 + 2.8*(i+1.0)/(args.ntbreak-1) \
                    + rand(nwalkers)-0.5
        ptr += 1
    for i in range(args.ntbreak):
        p0[:,ptr] = -1 + rand(nwalkers)-0.5
        ptr += 1
    for i in range(args.nav-1):
        p0[:,ptr] = 1.0 / (avlim[1] - avlim[0]) \
                    + 0.2*(rand(nwalkers)-0.5)
        ptr += 1
    fp = open(args.outname, "w")
    fp.close()
else:
    print "Reading saved MCMC chain..."
    nread = 0
    fp = open(args.outname, "r")
    chaindat = fp.read().split('\n')
    fp.close()
    while '' in chaindat:
        chaindat.remove('')
    for p, line in zip(p0, chaindat[-nwalkers-1:]):
        p[:] = [float(f) for f in line.split()[1:]]
    nread = len(chaindat) // nwalkers
    print "  read {:d} iterations".format(nread)
    fp.close()

# Do the MCMC, saving as we do
print ("Starting MCMC with {:d} walkers, {:d} iterations " +
       "remain...").format(nwalkers, niter-nread)
callctr = Value('i', 0)
sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob,
                                threads=cpu_count())
for result in sampler.sample(p0, iterations=niter-nread, storechain=False):
    position = result[0]
    fp = open(args.outname, "a")
    for k in range(position.shape[0]):
        fp.write("{0:4d}".format(k))
        for j in range(position.shape[1]):
            fp.write("  {:f}".format(position[k,j]))
        fp.write("\n")
    fp.close()
