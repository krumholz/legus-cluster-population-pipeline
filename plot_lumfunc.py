import numpy as np
import os.path as osp
from readphot import readphot
from slugpy.cluster_slug import cluster_slug
from slugpy import read_cluster
from completeness import completeness
from copy import deepcopy

# List of photometry files for observations
photfiles = [
    'phot/ngc628c_cluster_legus_avgapcor_PadAGB_MWext_16July15.tab'
    #,
    #'phot/ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab'
]

# Use the full catalog, or only class 1 - 3.5
fullcatalog = False

# Name of cluster_slug library
libname = '/Users/krumholz/Data/legus/lib/modp020_kroupalim_MW_phi0.50'

# Name of the directory where kernel density representations are
# stored
kddir = '/Users/krumholz/Data/legus/clusterpop'

# Minimum completeness level we accept
mincomp = 0.5
#mincomp = 0.0

# Read simulation library; set up convenient shorthands
try:
    libdat
except NameError:
    print "Reading slug simulation library {:s}".format(libname)
    libdat = read_cluster(libname)
    m_lib = libdat.actual_mass
    t_lib = libdat.time
    av_lib = libdat.A_V

    # Set sample density of simulation library
    def sample_density(physprop):
        logm = physprop[:,0]
        logt = physprop[:,1]
        sden = np.ones(len(logm))
        sden[logm > 4] = sden[logm > 4] * 1.0/10.**(logm[logm > 4]-4)
        sden[logt > 8] = sden[logt > 8] * 1.0/10.**(logt[logt > 8]-8)
        return sden

# Read metadata from the photometry files
allfilters = []
for f in photfiles:
    data = readphot(f, metaonly=True)
    for flt in data['allfilters']:
        if flt not in allfilters:
            allfilters.append(flt)
nf = len(allfilters)

# Read the input photometry files; apply cuts to the classification
# and remove duplicates to produce the final observed photometry list
# we'll be using; in the process, compute the observational
# completeness for each cluster, and for the library at each possible
# combination of field and filter set
try:
    p_obs_dat
except NameError:
    galnames = []
    radec = []
    phot = np.zeros((0,nf))
    photerr = np.zeros((0,nf))
    detect = np.zeros((0,nf), dtype=bool)
    mfit = np.zeros(0)
    ffidx_tmp = np.zeros(0, dtype=int)
    ffinfo = []
    p_obs_dat = np.zeros(0)
    p_obs_lib = np.zeros((0, libdat.phot_neb_ex.shape[0]))
    nread = 0
    n_kd = 0
    cid_to_use = []
    for f in photfiles:

        # Read photometry of the clusters we want
        print "Reading photometry file {:s}".format(f)
        if not fullcatalog:
            data = readphot(f)
        else:
            data = readphot(f, classcut=[-1,3.5])
        nread += len(data['phot'])
        galnames.append(data['name'])

        # Build the completeness object for this field and get
        # completeness estimate for all clusters
        print "Computing completeness"
        cmpl = completeness(data['name'].split('_')[0], 
                            data['allfilters'],
                            data['distmod'], prefix='cmpdat', 
                            viscat=not fullcatalog)
        p_obs_dat_file = cmpl.pobs(data['phot'], detect=data['detect'],
                                   filters=data['filters'])

        # Read the KD representation for this field
        kdfile = data['name']+'_kdrep.npz'
        if kddir is not None:
            kdfile = osp.join(kddir, kdfile)
        kd = np.load(kdfile, mmap_mode='r')

        # Now we do quality control on the data; we want to throw out:
        # (1) clusters that are duplicates of ones stored in other files;
        # (2) clusters so far from the library that we couldn't build
        #     meaningful KD representations for them
        # (3) clusters that are so far down the completeness that we don't
        #     believe we can make a reasonable completeness correction for
        #     them

        # Check RA and DEC against already-ingested clusters
        unqflag = np.zeros(len(data['phot']), dtype='bool')
        rad = zip(data['ra'], data['dec'])
        for i, rd in enumerate(rad):
            if rd not in radec:
                unqflag[i] = True
                radec.append(rd)

        # Check that clusters have KD representations that are not None
        kdflag = np.zeros(len(data['phot']), dtype='bool')
        for i, cid in enumerate(data['cid']):
            kdflag[i] = kd['w'+str(cid)].size > 0

        # Check that completeness is acceptable
        cmpflag = p_obs_dat_file > mincomp

        # Construct the set of clusters that pass all quality criteria;
        # count how many of them there are, and the number of points in
        # their KD representations
        goodidx = np.logical_and.reduce((unqflag, kdflag, cmpflag))
        ncl = np.sum(goodidx)
        for i, cid in enumerate(data['cid']):
            if goodidx[i]:
                n_kd += kd['w'+str(cid)].size    

        # Print status
        print "Read {:d} clusters:".format(len(data['phot']))
        print "   {:d} duplicates".\
            format(np.sum(np.logical_not(unqflag)))
        print "   {:d} bad KD representations".\
            format(np.sum(np.logical_not(kdflag)))
        print "   {:d} below completeness cut".\
            format(np.sum(np.logical_not(cmpflag)))
        print "Retaining {:d}".format(ncl)

        # Figure out the mapping between the numerical indices of the
        # filters used for this photometry file and the global indexing
        # we're using for the merged photometry over all files
        fidx = np.zeros(len(data['allfilters']), dtype=int)
        for i in range(fidx.size):
            fidx[i] = allfilters.index(data['allfilters'][i])

        # Arrange the photometric information for this field and add it to
        # the global photometry list
        phottmp = np.zeros((ncl, nf))
        photerrtmp = np.zeros((ncl, nf))
        detecttmp = np.zeros((ncl, nf), dtype=bool)
        mfittmp = data['mfit'][goodidx]
        for i, j in enumerate(fidx):
            phottmp[:,j] = data['phot'][goodidx,i]
            photerrtmp[:,j] = data['photerr'][goodidx,i]
            detecttmp[:,j] = data['detect'][goodidx,i]
        phot = np.concatenate((phot, phottmp))
        photerr = np.concatenate((photerr, photerrtmp))
        detect = np.concatenate((detect, detecttmp))
        mfit = np.concatenate((mfit, mfittmp))

        # Record the IDs of the clusters we're using
        cid_to_use.append(data['cid'][goodidx])

        # Store the observational completeness estimate for the data
        p_obs_dat = np.concatenate((p_obs_dat,
                                    p_obs_dat_file[goodidx]))

        
    # Now we need to get the observational completeness for the
    # library. We have to evaluate this for every combination of field
    # and filter set, so, as a first step, figure out how many
    # distinct filter sets are present in this field, and record
    # information describing them
    for fs in data['filtersets']:
        ffinfo.append((data['name'], fs))

    # For each cluster, record which combination of field and data set
    # it is using
    ffidxtmp = []
    for g in np.where(goodidx)[0]:
        ffidxtmp.append(
            ffinfo.index((data['name'], data['filters'][g])))
    ffidx_tmp = np.concatenate((ffidx_tmp, ffidxtmp))

    # Now get the observational completeness of the library clusters
    # for all filter sets in this field; note that we need to extract
    # from the library just those filters that match the ones used for
    # this field; insert this completeness into ther cluster_slug
    # object
    print "Getting observational completeness for slug library"
    libphot = np.zeros((libdat.phot_neb_ex.shape[0],
                        len(data['allfilters'])))
    for i, f in enumerate(data['allfilters']):
        libphot[:,i] \
            = libdat.phot_neb_ex[:,libdat.filter_names.index(f)]
    for fs in data['filtersets']:
        print "   filter set: "+str(fs)
        p_obs_lib_file = cmpl.pobs(libphot, filters=fs).\
                         reshape(1, len(libphot))
        p_obs_lib_file[p_obs_lib_file < mincomp] = 0.0
        p_obs_lib = np.concatenate(
            (p_obs_lib, p_obs_lib_file))
    

# Create ranked list of observed magnitudes in each filter
obs_mag_rank = []
for i in range(nf):
    mag = phot[detect[:,i],i]
    mag = np.sort(mag)[::-1]
    obs_mag_rank.append(mag)

# Create the cluster_slug object without observational completeness
try:
    cs
except NameError:
    cs = cluster_slug(lib=libdat, bw_phot=0.1,
                      sample_density=sample_density)
    cs_obs = cluster_slug(lib=deepcopy(libdat), bw_phot=0.1,
                          sample_density=sample_density)

# Class to set priors
class priorfunc(object):
    def __init__(self, log_mstar, mslope, log_tbreak, tslope,
                 AV_mean, AV_disp):
        self.mstar = 10.**log_mstar
        self.mslope = mslope
        self.tbreak = 10.**log_tbreak
        self.tslope = tslope
        self.AV_mean = AV_mean
        self.AV_disp = AV_disp
    def prior(self, physprop):
        m = 10.**physprop[:,0]
        t = 10.**physprop[:,1]
        av = physprop[:,2]
        mprior = m**(self.mslope+1)*np.exp(-m/self.mstar)
        tprior = (t/self.tbreak)**(self.tslope+1)
        tprior[t < self.tbreak] = t[t < self.tbreak]/self.tbreak
        avprior = np.exp(-(av - self.AV_mean)**2 /
                         (2.0*self.AV_disp**2))
        return mprior*tprior*avprior
    
# Define a function that returns the luminosity function in a
# particular photometric band for a given input set of cluster
# population parameters
def lumfunc(filter_name, log_mstar, mslope, log_tbreak, tslope,
            AV_mean, AV_disp, pobs=None, ngrid=128,
            lmin=None, lmax=None):
    """
    Returns the luminosity function in the specified filter for a
    given set of cluster population parameters

    Parameters:
       filter_name : string
          name of the filter in which to compute the luminosity
          function
       log_mstar : float
          log_10 mass in Schechter function
       mslope : float
          slope of mass function dn / dm
       log_tbreak : float
          log_10 of age at which cluster disruption begins
       tslope : float
          slope of age distribution dn / dt
       AV_mean : float
          mean extinction
       AV_disp : float
          dispersion of extinctions
       pobs : array
          probability that each cluster will be observed
       ngrid : int
          number of grid points in luminosity function
       lmin : float
          minimum luminosity in output grid
       lmax : float
          maximum luminosity in output grid

    Returns:
       lumgrid : array
          array of luminosities at which the PDF has been computed
       lumfunc : array
          value of luminosity function at each point of lumgrid 
    """

      # Point to correct cs object
    if pobs is None:
        cstmp = cs
    else:
        cstmp = cs_obs

    # Set priors
    prior = priorfunc(log_mstar, mslope, log_tbreak, tslope,
                      AV_mean, AV_disp)
    cstmp.priors = prior.prior

    # Add filter
    cstmp.add_filters([filter_name], pobs = pobs)

    # Compute PDF of photometry, marginalising over physical
    # properties
    lumgrid, lumfunc \
        = cstmp.mpdf_gen(None, None, margindim=[0,1,2], ngrid=ngrid,
                         qmin=lmin, qmax=lmax, filters=[filter_name])

    # Return
    return lumgrid, lumfunc
          
